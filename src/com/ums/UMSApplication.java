package com.ums;

import java.io.IOException;
import java.util.Optional;

import com.ums.db.UMSDBException;
import com.ums.model.User;
import com.ums.ui.UserEditUIController;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class UMSApplication extends Application {

	private Stage primaryStage;
	private BorderPane mainUI;
	private AnchorPane userUI;
	private DataSource dataSource;
	private static UMSApplication instance;

	@Override
	public void start(Stage primaryStage) {
		instance = this;
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("User Management System");

		try {
			dataSource = new DataSource();
			initRootLayout();
			showUserUI();
		} catch (UMSDBException e) {
			System.err.println(e.getMessage());
		}
	}

	// Initializes the root layout.
	public void initRootLayout() {
		try {
			// Load root layout from fxml file.
			mainUI = (BorderPane) FXMLLoader.load(getClass().getResource("ui/MainUI.fxml"));

			// Show the scene containing the root layout.
			Scene scene = new Scene(mainUI);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Shows the user UI inside the root layout.
	public void showUserUI() {
		try {
			// Load userUI
			userUI = (AnchorPane) FXMLLoader.load(getClass().getResource("ui/UserUI.fxml"));
			// Set userUI into the center of root layout.
			mainUI.setCenter(userUI);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Returns the main stage.
	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public static UMSApplication getInstance() {
		return instance;
	}

	public static void main(String[] args) {
		launch(args);
	}

	public boolean showUserEditUI(User user) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(UMSApplication.class.getResource("ui/UserEditUI.fxml"));
			AnchorPane page = (AnchorPane) loader.load();

			// Create the dialog Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Ajouter/Modifier un utilisateur");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			dialogStage.setScene(new Scene(page));

			UserEditUIController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			controller.setUser(user); // Set the user into the controller.

			// Show the dialog and wait until the user closes it
			dialogStage.showAndWait();
			return controller.isValiderClicked();
		} catch (IOException e) {
			System.err.println(e.getMessage());
			return false;
		}
	}

	public static Optional<ButtonType> alert(AlertType alertType, String title, String headerText, String contentText) {
		Alert alert = new Alert(alertType);
		alert.setTitle(title);
		alert.setHeaderText(headerText);
		alert.setContentText(contentText);
		return alert.showAndWait();
	}

}
