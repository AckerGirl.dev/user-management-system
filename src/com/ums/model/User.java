package com.ums.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class User {

	private int id;
	private StringProperty nom;
	private StringProperty prenom;
	private StringProperty email;
	private StringProperty telephone;
	private StringProperty login;
	private StringProperty password;
	private StringProperty role;

	public User() {
		this("", "", "", "", Role.SIMPLE_USER);
		this.login = new SimpleStringProperty("");
		this.password = new SimpleStringProperty("");
	}

	public User(String nom, String prenom, String email, String telephone, Role role) {
		this.nom = new SimpleStringProperty(nom);
		this.prenom = new SimpleStringProperty(prenom);
		this.email = new SimpleStringProperty(email);
		this.telephone = new SimpleStringProperty(telephone);

		// login et password par défaut ...
		this.login = new SimpleStringProperty(prenom.trim().toLowerCase() + "." + nom.trim().toLowerCase());
		this.password = new SimpleStringProperty("p@Ss3R");
		this.role = new SimpleStringProperty(role.getValue());
	}

	public User(int id, String nom, String prenom, String email, String telephone, String login, String password,
			String role) {
		this.id = id;

		this.nom = new SimpleStringProperty(nom);
		this.prenom = new SimpleStringProperty(prenom);
		this.email = new SimpleStringProperty(email);
		this.telephone = new SimpleStringProperty(telephone);
		this.login = new SimpleStringProperty(login);
		this.password = new SimpleStringProperty(password);

		this.role = new SimpleStringProperty(role);
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	/**
	 * @return the nom
	 */
	public StringProperty getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(StringProperty nom) {
		this.nom = nom;
	}

	/**
	 * @return the prenom
	 */
	public StringProperty getPrenom() {
		return prenom;
	}

	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(StringProperty prenom) {
		this.prenom = prenom;
	}

	/**
	 * @return the email
	 */
	public StringProperty getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(StringProperty email) {
		this.email = email;
	}

	/**
	 * @return the telephone
	 */
	public StringProperty getTelephone() {
		return telephone;
	}

	/**
	 * @param telephone the telephone to set
	 */
	public void setTelephone(StringProperty telephone) {
		this.telephone = telephone;
	}

	/**
	 * @return the login
	 */
	public StringProperty getLogin() {
		return login;
	}

	/**
	 * @param login the login to set
	 */
	public void setLogin(StringProperty login) {
		this.login = login;
	}

	/**
	 * @return the password
	 */
	public StringProperty getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(StringProperty password) {
		this.password = password;
	}

	/**
	 * @return the role
	 */
	public StringProperty getRole() {
		return role;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole(StringProperty role) {
		this.role = role;
	}
}
