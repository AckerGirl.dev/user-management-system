package com.ums;

import java.util.List;

import com.ums.db.UMSDBException;
import com.ums.db.dao.IUserDao;
import com.ums.db.dao.UserDaoImpl;
import com.ums.model.User;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class DataSource {

	ObservableList<User> users = FXCollections.observableArrayList();
	private IUserDao dao;

	public DataSource() throws UMSDBException {
		dao = new UserDaoImpl();
		listUsers();
	}

	public ObservableList<User> getUsers() {
		return users;
	}

	public void createUser(User user) throws UMSDBException {
		dao.create(user);
		this.users.add(user);
	}

	public void updateUser(User user, int selectedIndex) throws UMSDBException {
		dao.update(user);
		users.remove(selectedIndex);
		users.add(selectedIndex, user);
	}

	private void listUsers() throws UMSDBException {
		List<User> users = this.dao.list();
		this.users.addAll(users);
	}

	public void deleteUser(int selectedIndex) throws UMSDBException {
		User user = users.get(selectedIndex);
		users.remove(user);
		dao.delete(user.getId());
	}

	public User readUser(Integer id) throws UMSDBException {
		return dao.read(id);
	}
}
