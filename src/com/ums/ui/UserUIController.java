package com.ums.ui;

import com.ums.UMSApplication;
import com.ums.db.UMSDBException;
import com.ums.model.User;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;

public class UserUIController {
	@FXML
	private TableView<User> userTable;
	@FXML
	private TableColumn<User, String> nomColumn;
	@FXML
	private TableColumn<User, String> prenomColumn;

	@FXML
	private Label nomLabel;
	@FXML
	private Label prenomLabel;
	@FXML
	private Label emailLabel;
	@FXML
	private Label telephoneLabel;
	@FXML
	private Label loginLabel;
	@FXML
	private Label passwordLabel;
	@FXML
	private Label roleLabel;

	// the data source ...
	// private DataSource dataSource;

	public UserUIController() {
	}

	@FXML
	private void initialize() {

		refreshTable();

		// Clear the user details form
		displayUserDetails(null);
		// Add a changeListener to the userTable
		addChangeListener();

	}

	/**
	 * Fills all text fields to show details about the user. If the specified user
	 * is null, all text fields are cleared. *
	 * 
	 * @param user the user or null
	 */
	private void displayUserDetails(User user) {
		if (user != null) {
			// Fill the labels with info from the user object.
			this.nomLabel.setText(user.getNom().get());
			this.prenomLabel.setText(user.getPrenom().get());
			this.emailLabel.setText(user.getEmail().get());
			this.telephoneLabel.setText(user.getTelephone().get());
			this.loginLabel.setText(user.getLogin().get());
			this.passwordLabel.setText("********");
			this.roleLabel.setText(user.getRole().get());
		} else {
			// User is null, remove all the text.
			this.nomLabel.setText(null);
			this.prenomLabel.setText(null);
			this.emailLabel.setText(null);
			this.telephoneLabel.setText(null);
			this.loginLabel.setText(null);
			this.passwordLabel.setText(null);
			this.roleLabel.setText(null);
		}
	}

	/**
	 * Surveille les changements sur la table et affiche les informations dans le
	 * formulaire
	 */
	private void addChangeListener() {
		userTable.getSelectionModel().selectedItemProperty()
				.addListener((observable, oldValue, newValue) -> displayUserDetails(newValue));
	}

	/**
	 * Called when the user clicks on the Supprimer button.
	 * 
	 * @throws UMSDBException
	 */
	@FXML
	private void handleSupprimerUser() throws UMSDBException {
		int selectedIndex = userTable.getSelectionModel().getSelectedIndex();
		if (selectedIndex >= 0) {
			UMSApplication.getInstance().getDataSource().deleteUser(selectedIndex);
			;
		} else {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Warning ...");
			alert.setHeaderText("Alerte !");
			alert.setContentText("Veuillez sélectionner un utilisateur svp !");
			alert.showAndWait();
		}
	}

	@FXML
	private void handleNouveauUser() throws UMSDBException {
		User user = new User();

		boolean validerClicked = UMSApplication.getInstance().showUserEditUI(user);
		if (validerClicked) {
			UMSApplication.getInstance().getDataSource().createUser(user);
		}
	}

	@FXML
	private void handleModifierUser() throws UMSDBException {
		User selectedUser = userTable.getSelectionModel().getSelectedItem();
		int selectedIndex = userTable.getSelectionModel().getSelectedIndex();

		if (selectedUser != null) {
			boolean validerClicked = UMSApplication.getInstance().showUserEditUI(selectedUser);
			if (validerClicked) {
				UMSApplication.getInstance().getDataSource().updateUser(selectedUser, selectedIndex);
				userTable.refresh();
				userTable.getSelectionModel().select(selectedIndex);
				displayUserDetails(selectedUser);

			}
		} else {
			// Nothing selected.
			UMSApplication.alert(AlertType.WARNING, "Aucune sélection", "Aucun utilisateur n'a été sélectionné !",
					"Veuillez choisir un utilisateur svp !.");

		}
	}

	private void refreshTable() {
		nomColumn.setCellValueFactory(cellData -> cellData.getValue().getNom());
		prenomColumn.setCellValueFactory(cellData -> cellData.getValue().getPrenom());

		// dataSource = new DataSource();

		// userTable.setItems(dataSource.getUsers());

		// Add observable list data to the table
		userTable.setItems(UMSApplication.getInstance().getDataSource().getUsers());
	}

	@FXML
	private void handleRechercherUser() throws UMSDBException {

		TextInputDialog dialog = new TextInputDialog();
		dialog.setTitle("Rechercher un utilisateur");
		dialog.setHeaderText(null);
		dialog.setContentText("Veuillez saisir l'id de l'utilisateur");
		String reponse = dialog.showAndWait().get();

		try {
			Integer id = Integer.parseInt(reponse);
			User user = UMSApplication.getInstance().getDataSource().readUser(id);
			if (user == null) {
				UMSApplication.alert(AlertType.INFORMATION, "Rechercher un utilisateur", null,
						"L'utilisateur recherché n'existe pas !");
			} else {
				UMSApplication.getInstance().showUserEditUI(user);
			}

		} catch (NumberFormatException e) {
			UMSApplication.alert(AlertType.ERROR, e.getClass().toString(), null,
					"Veuillez saisir un nombre entier svp !");
		}

	}
}
